<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends CI_Model
{
	private $table_name = 'contacts';
	
	function __construct()
	{
		parent::__construct();
		
	}
	
	function get_contact_list($start=0, $count=100)
	{
		
	}
	
	function add_contact($data)
	{
		$data['created'] = date('Y/m/d H:i:s');

		if ($this->db->insert($this->table_name, $data)) {
			$contact_id = $this->db->insert_id();

			return array('contact_id' => $contact_id);
		}
		return NULL;
	}
}