<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('tank_auth');
		
		$this->load->model('admin_m');
	}
	
	function _remap($method) {
		$this->load->view('header_v');
		$this->{$method}();
		$this->load->view('footer_v');
	}
	
	function index()
	{
	
	}
	
	function about()
	{
		$this->load->view('about_v');
	}
	
	function contact()
	{
		$data = $this->_proc_contact_add();
		$data['post_key'] = "contact";
		$this->load->view('contact_v', $data);
	}
	
	function clients()
	{
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$this->load->view('clients_v', $data);
		}
	}
	
	private function &_proc_contact_add()
	{
		$this->form_validation->set_rules('yourname', 'Your name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('company', 'Company', 'trim|required|xss_clean');
		$this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('comments', 'Comments', 'trim|required|xss_clean');
		
		$data = array();
		if ($this->form_validation->run())
		{
			$data['visitor'] = $this->form_validation->set_value('yourname');
			$data['company'] = $this->form_validation->set_value('company');
			$data['phone'] = $this->form_validation->set_value('phone_number');
			$data['email'] = $this->form_validation->set_value('email_address');
			$data['comments'] = $this->form_validation->set_value('comments');

			/*if (!is_null($res = $this->admin_m->add_contact($data)))
			{
				// Send mail
				$data['show_message'] = "Successfully submit!";
				$data['contact_id'] = $res['contact_id'];
			}*/
			

		}		
		return $data;
	}
	
	/**
	 * Send email message of given type (activate, forgot_password, etc.)
	 *
	 * @param	string
	 * @param	string
	 * @param	array
	 * @return	void
	 */
	function _send_email($type, $email, &$data)
	{
		$this->load->library('email');
		$this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
		$this->email->to($email);
		$this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
		$this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
		$this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
		$this->email->send();
	}
}