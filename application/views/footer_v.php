	
	
<div id="footer">
	<div class="stock_footer">
		<div class="container">
			<ul>
				<li>
				<strong>DJIA:</strong><span class="standing djiaLastPrice"></span>
				              <span class="change"><span class="djiaChangeAmt"></span> (<span class="djiaChangePct"></span>)</span>
				</li>
				<li>
				<strong>NASDAQ:</strong><span class="standing nasdaqLastPrice"></span>
				              <span class="change"><span class="nasdaqChangeAmt"></span> (<span class="nasdaqChangePct"></span>)</span>
				</li>
				<li>
				<strong>S&amp;P 500:</strong><span class="standing sandpLastPrice"></span>
				              <span class="change"><span class="sandpChangeAmt"></span> (<span class="sandpChangePct"></span>)</span>
				</li>
			</ul>
			<div class="stock_time lastUpdate"></div>
			<div class="button">
				<a target="_parent" class="green-button" href="https://www.tdameritrade.com/o.cgi?a=WEB&amp;p=https%3A%2F%2Fwwwna.tdameritrade.com%2Fcgi-bin%2Fapps%2FAccountApServlet%3Fsegment%3Dtdameritrade" title="Open New Account"><span>Open New Account</span></a>
			</div>
			<div class="stock_offer">
				<div><h4>TRADE COMMISSION-FREE FOR 60 DAYS</h4>
				<p>PLUS <span>GET UP TO $600</span></p><a target="_parent" href="https://www.tdameritrade.com/specialoffer.page" title="Offer details">Offer details</a>
				</div>
			</div>
		</div>
	
		<div class="utility-footer container">
			<ul class="container">
				<li class="social">
					<a href="http://www.youtube.com/tdameritrade" title="Youtube" target="_blank" class="youtube"></a><a href="https://www.facebook.com/pages/TD-Ameritrade/323110354399964" title="Facebook" target="_blank" class="facebook"></a><a href="http://twitter.com/tdameritrade" title="Twitter" target="_blank" class="twitter"></a>
				</li>
				<li class="alpha">
					<a class="icon" title="Feedback" href="javascript:void(0);" onclick="feedbackLink()"><img class="rotating-sign" src="<?php echo IMG_DIR."/";?>rotating-plus-sign-white.gif" height="9" width="9"><span class="text">Feedback</span></a>
				</li>
				<li class="">
				<a target="_self" class="" href="<?php echo site_url("");?>" title="Home">Home</a>
				</li>
				<li class="">
				<a target="_self" class="" href="<?php echo site_url("page/about");?>" title="About">About</a>
				</li>
				<li class="">
				<a target="_self" class="" href="<?php echo site_url("page/clients");?>" title="Clients">Clients</a>
				</li>
				<li class="">
				<a target="_self" class="" href="<?php echo site_url("page/contact");?>" title="Contact">Contact</a>
				</li>
			</ul>
		</div>	
	</div>
</div>
</body>

</html>