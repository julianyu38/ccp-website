	<div class="frontpage-notification about">		
	
	</div>	
	<div id="more-info" class="box">
		<div id="info">
		
			<div class="intro">
				<span>About</span>
				
			</div>
			<div class="part jason defelice">
				<div class="title">
					<h2>Jason DeFelice</h2>
					
		            <p>Jason DeFelice is the Co-Founder of Cove Capital Partners, LLC, and has been in the Wealth Management Business since 1996.  He prides himself on providing excellent advice and customer service while exceeding his clients expectations.  Jason has climbed through the ranks since starting out with Paine Webber, Inc.  and has worked in Atlanta, New York City and New Jersey,  with clients throughout the country. </p>
		            <p>Jason was also VP of Investments at JP Turner, where his trading acumen lead him to become a hedge fund portfolio manager.  Understanding the unique differences of his clients, along with his in depth market analysis has enabled Jason to build long term relationships with his clients.  He understands the risk/reward paradigm, and has guided his clients successfully through the internet tech bubble as well as the recent recession.  He has worked from Mt. Pleasant, SC since 2004.</p>
		            <p>Jason holds his Series 7. 63 and 65 securities licenses.  He resides in Mt. Pleasant, SC, with his hot young wife Kristi, and his two little girls Jayce and Sloane.</p>		              
		        </div>
	        </div>
	        
	        <div class="part loren ziff">
	        	<div class="title">
	        		<h2>Loren Ziff</h2>
	        		
	        		<p>Loren Ziff is the Co-Founder of Cove Capital Partners, LLC, and has been a successful entrepreneur and investor for over 25 years.  Loren brings a wide range of opportunistic experience to Cove Capital, with a solid record of success, creativity and hard work.   Loren prides himself in having grown up in public housing in New Haven, Ct, and the values that come from such modest beginnings.</p>
	        		<p>Loren received his undergraduate degree in Economics from Northeastern University in 1984, where he was a Charles Irwin Travelli Scholar and a member of the Varsity Swim Team.  Loren then proceeded to Duke University's Fuqua School of Business, graduating with his MBA in 1986. He has since then spent most of his career in the real estate industry.   Early in his career, he managed $3 billion of distressed assets for the Federal Government during the Savings & Loan crisis, including running two failed thrifts in the Southeast.  He also was the President and co-founder of Ziff Properties, Inc., taking it from zero to over $100 million in assets.  This lead to Loren creating Eastrock Properties, LLC, a boutique real estate service and investment firm.</p>
	        		<p>Loren is known for bringing creative positive energy into everything he touches, and his hallmark is finding win/win solutions to challenging opportunities.  Loren is also actively engaged in the community, serving on the Executive Committee for Spoleto Festival USA, Chairman of Operation Sight, Co-Founder & Co-Chairman of Push-Up & Up, Inc. and is actively engaged at The College of Charleston as a mentor and on various boards.  He is married to Mindelle and they and their 3 children reside on Sullivan's Island, SC.</p>	        		
	        	</div>
	        	
	        	<img src="<?php echo IMG_DIR."/"; ?>loren_ziff.jpg" height="236" width="171">	        	 
	        </div>
	    </div>
	</div>