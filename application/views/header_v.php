<!doctype html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8"/>
	<title><?php echo SITE_NAME; ?></title>
	
	<link href="<?php echo CSS_DIR."/"; ?>style.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="<?php echo CSS_DIR."/"; ?>font.css" rel="stylesheet" type="text/css" media="screen" />
	<script src="<?php echo JS_DIR."/"; ?>jquery.js"></script>
	<script src="<?php echo JS_DIR."/"; ?>main.js"></script>
<script type="text/javascript">
<!--
	function confirm_logout() {
		if(confirm("Do you want to logout?")) { 
			window.location.href = "<?php echo site_url("auth/logout");?>";
		}
	}
//-->
</script>

</head>

<body>
	<div id="background"></div>
	
	<div id="header">
		<h1><a href="https://ccp.com/">Stripe</a></h1>
		<div class="navigation">
<?php
	//if($this->tank_auth->is_logged_in()) { 
?>
			<!-- <div class="button"><a id="signout" href="javascript:confirm_logout()"><span>Sign Out</span></a></div> -->
<?php
	//} else {
?>				
            <div class="button"><a id="signin" href="<?php echo site_url("page/clients");?>"><span>Sign In</span></a></div>
<?php
	//}
?>
            <ul class="global">
                <li><a href="<?php echo site_url("");?>">Home</a></li> 
                <li><a href="<?php echo site_url("page/about");?>">About</a></li>
                <li><a href="<?php echo site_url("page/clients");?>">Clients</a></li>
                <li><a href="<?php echo site_url("page/contact");?>">Contact</a></li>
            </ul>
        </div>
		
	</div> <!-- END HEADER -->
	
	
	
