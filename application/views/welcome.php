
	<div class="frontpage-notification">		
	
	</div>
	
	<div id="more-info" class="box">
		<div id="info">
		
			<div class="intro">
				<span>Introduction</span>
				
			</div>
			<div class="part">
				<div class="title">
		            <p>Cove Capital Partners is a boutique Investment Advisory business focused on building long-term relationships through great communication, personalized service and exceptional performance.</p>  
		            <p>We are not a one size fits all business.  </p>
		            <p>We take pride in defining success with each client based on their risk tolerance, liquidity needs and short and long term objectives.</p>  
		            <p>We use technical and fundamental analysis in every trade in order to optimize our recommendations with your goals and objectives.</p>
		        </div>
	        </div>
	    </div>
	</div>