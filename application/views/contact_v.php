<?php
$yourname = array(
	'name'	=> 'yourname',
	'id'	=> 'yourname',
	'value' => set_value('yourname'),
	'maxlength'	=> 24,
	'size'	=> 30,		
);
$company = array(
	'name'	=> 'company',
	'id'	=> 'company',
	'value'	=> set_value('company'),
	'maxlength'	=> 32,
	'size'	=> 30,
);
$phone_number = array(
	'name'	=> 'phone_number',
	'id'	=> 'phone_number',
	'value' => set_value('phone_number'),
	'maxlength'	=> 16,
	'size'	=> 30,
);
$email_address = array(
	'name'	=> 'email_address',
	'id'	=> 'email_address',
	'value' => set_value('email_address'),
	'maxlength'	=> 32,
	'size'	=> 30,
);
$comments = array(
	'name'	=> 'comments',
	'id'	=> 'comments',
	'value' => set_value('comments'),
	'maxlength'	=> 64,
	'size'	=> 30,
);
?>	
<?php //echo form_open_multipart($this->uri->uri_string()); ?>

		<div id="info">
<?php
if (!empty($show_message)) {	
	echo "<h4 class='alert_success'>".$show_message."</h4>";
} 
?>		
	<div class="frontpage-notification contact">		
	
	</div>
<div id="more-info" class="box">
		
			<div class="intro">
				<span>Contact</span>
			
			</div>
			<div class="part contact form">
				<div class="inner">
					<div id="contact-form">
						<div id="contact-forms">
							<div><?php echo form_label('Your name', $yourname['id']); ?></div>
							<div><?php echo form_input($yourname); echo form_error($yourname['name']); ?></div>
						</div>
						<div id="contact-forms">
							<div><?php echo form_label('Company', $company['id']); ?></div>
							<div><?php echo form_input($company); echo form_error($company['name']); ?></div>
						</div>
						<div id="contact-forms">
							<div><?php echo form_label('Phone number', $phone_number['id']); ?></div>
							<div><?php echo form_input($phone_number); echo form_error($phone_number['name']); ?></div>
						</div>
						<div id="contact-forms">
							<div><?php echo form_label('Email', $email_address['id']); ?></div>
							<div><?php echo form_input($email_address); echo form_error($email_address['name']); ?></div>
						</div>
						<div id="contact-forms">
							<div><?php echo form_label('Comments', $comments['id']); ?></div>
							<div><?php echo form_textarea($comments); echo form_error($comments['name']); ?></div>
						</div>
						<div id="contact-forms">
							<?php echo form_submit('submit', 'Submit'); ?>
						</div>
					</div>
					<div class="title">
						<h2>Jason DeFelice</h2>
						
			            <p>Email address : <a href="mailto:Jason@CoveCapitalPartners.com">Jason@CoveCapitalPartners.com</a><br>
						Telephone : 843.606.4010<br>
						Mobile : 843.469.0499 (Mobile)</p><br>
	
						<h2>Loren Ziff</h2>
						
			            <p>Email address : <a href="mailto:Loren@CoveCapitalPartners.com">Loren@CoveCapitalPartners.com</a><br>
						Telephone : 843.606.4010<br>
						Mobile : 843.270.6000 (Mobile)</p><br>
	
						<h2>Address</h2>
						
			            <p>856 Lowcountry Boulevard, Suite 101<br>
						Mt. Pleasant, SC 29464<br>
						Fax: 843.723.7300</p>
			        </div>	    
			        
			        <div class="clear"></div>    					
				</div>
				
				
	
	        </div>	        
	        
	    </div>
	</div>
<?php //echo form_close(); ?>