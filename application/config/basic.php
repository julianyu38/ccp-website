<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	$config['upload_path']		= "./include/ufile";
	$config['upload_movietype']	= 'mp4|flv|3gp';
	$config['upload_moviesize']	= 100 * 1024; // 100M
	
	$config['upload_imgtype']	= 'gif|jpg|png|bmp|jpeg|jpe';
	$config['upload_imgsize']	= 5 * 1024; // 5M
	$config['upload_thumb_mw']	= '80';
	$config['upload_thumb_mh']	= '60';

	$config['upload_kmltype']	= 'kml';
	$config['upload_kmlsize']	= 10 * 1024; // 10M
	
	$config['main_category'] = array(
		/*'users'	=>	'User Manage',
		'posts' =>	'Posts',*/
	);
	
	$config['max_count_per_page'] = 5;