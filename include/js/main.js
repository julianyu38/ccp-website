/**
 * Main javascript file.
 */

$(document).ready(function(){
	getStockTicker();
});

function indexViewModel()
{
	this.djiaChangePct="";
	this.djiaChangeAmt="";
	this.djiaLastPrice="";
	this.djiaChangeDir="";
	this.nasdaqChangePct="";
	this.nasdaqChangeAmt="";
	this.nasdaqLastPrice="";
	this.nasdaqChangeDir="";
	this.sandpChangePct="";
	this.sandpChangeAmt="";
	this.sandpLastPrice="";
	this.sandpChangeDir="";
	this.lastUpdate="";
}

function formatChangePct(a){return""+(Math.abs(Math.round(a*100)/100)+"%")};
function formatChangeAmt(a){return(a<0?"\u2212":"+")+Math.abs(Math.round(a*100)/100)};
function formatLastPrice(a){
	var c,b;
	a+="";
	c=a.split(".");
	a=c[0];
	b=c.length>1?"."+c[1]:"";
	for(c=/(\d+)(\d{3})/;c.test(a);)
		a=a.replace(c,"$1,$2");
	return a+b;
}
function formatChangeDir(a){return a>0?1:a<0?-1:0};
function formatDate(a){var c,b,e,f,k,g;b=a.getHours(a)>12?a.getHours(a)-12:a.getHours(a);f=a.getMinutes(a);e=a.getHours(a)>12?"pm":"am";k=a.getMonth(a)+1;c=a.getDate(a);g=a.getFullYear(a).toString().substr(2,2);a=a.toTimeString();a.indexOf("(")!==-1?(a=a.replace(/.*\(([A-Za-z ]+)\)/,"$1"),a.indexOf(" ")!==-1&&(a=a.replace(/(\w)\w+[ ]?/g,"$1"))):a=a.split(" ").pop();return"As of "+b+":"+f+e+" "+a+" "+k+"/"+c+"/"+g}

function dump(obj) {
	console.log(JSON.stringify(obj));
}

function get_json(_url, _type, _data, _func) {
	
	//	$.getJSON(_url, _data, _func);

	console.log("========= trying URL : " + _url);

	$.ajax({
		url: _url ,
		type: _type,
	//	contentType: "application/json",
		dataType: 'jsonp',
		data: _data,
		success: _func,
		error: function(jqXHR, exception) {
			dump(jqXHR);
		}

	});


}
function getStockTicker() 
{
	get_json(
		"https://mobileapi.tdameritrade.wallst.com/Quote/IndexPerformanceFooter?symbols=.DJI|.IXIC|.SPX&user_id=mobileapi&user_password=mobileapi",
		"POST",
		{},
		function(data){			
			dump(data);
			
			var dest = new indexViewModel();
			
			dest.djiaChangePct = formatChangePct(data.Quotes[0].ChangePct);
			dest.djiaChangeAmt = formatChangeAmt(data.Quotes[0].ChangeAmt);
			dest.djiaLastPrice = formatLastPrice(data.Quotes[0].LastPrice);
			dest.djiaChangeDir = formatChangeDir(data.Quotes[0].ChangeAmt);
			dest.nasdaqChangePct = formatChangePct(data.Quotes[1].ChangePct);
			dest.nasdaqChangeAmt = formatChangeAmt(data.Quotes[1].ChangeAmt);
			dest.nasdaqLastPrice = formatLastPrice(data.Quotes[1].LastPrice);
			dest.nasdaqChangeDir = formatChangeDir(data.Quotes[1].ChangeAmt);
			dest.sandpChangePct = formatChangePct(data.Quotes[2].ChangePct);
			dest.sandpChangeAmt = formatChangeAmt(data.Quotes[2].ChangeAmt);
			dest.sandpLastPrice = formatLastPrice(data.Quotes[2].LastPrice);
			dest.sandpChangeDir = formatChangeDir(data.Quotes[2].ChangeAmt);

			dest.lastUpdate = formatDate(new Date(data.Quotes[2].LastDate*1E3));

			$(".djiaLastPrice").html(dest.djiaLastPrice);
			$(".djiaChangeAmt").html(dest.djiaChangeAmt);
			$(".djiaChangePct").html(dest.djiaChangePct);

			$(".nasdaqLastPrice").html(dest.nasdaqLastPrice);
			$(".nasdaqChangeAmt").html(dest.nasdaqChangeAmt);
			$(".nasdaqChangePct").html(dest.nasdaqChangePct);

			$(".sandpLastPrice").html(dest.sandpLastPrice);
			$(".sandpChangeAmt").html(dest.sandpChangeAmt);
			$(".sandpChangePct").html(dest.sandpChangePct);

			$(".lastUpdate").html(dest.lastUpdate);
		}
	);	
}