<?php 

define('RT_PATH', '/website/ccp');

$base_url	= "http://".$_SERVER['HTTP_HOST'];
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

define('IMG_DIR',	$base_url.'include/images');
define('CSS_DIR',	$base_url.'include/css');
define('JS_DIR',	$base_url.'include/js');
//define('EDT_DIR', 	RT_PATH.'/include/fckeditor');
//define('EDT_UPLOAD_DIR', RT_PATH.'/include/ufile/');

define('SITE_NAME', 'Cove Capital Partners.LLC');
define('ADMIN_EMAIL', 'webmaster@ccp.com');